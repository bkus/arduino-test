#include <Arduino.h>
#include "pt.h"

// Write your code here

void setup()
{
    uint32_t ledPin = 8;
    pinMode(ledPin, OUTPUT);
    pinMode(13, OUTPUT);
}

void loop()
{
    uint32_t ledPin = 8;
    uint32_t sec = 4000;
    digitalWrite(ledPin, HIGH);
    delay(sec);
    digitalWrite(ledPin, LOW);
    delay(sec);
}